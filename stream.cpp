#include "stream.hpp"

void Stream::initAsound()
{
  int err;
  m_asoundinit = true;
  
  if ((err = snd_pcm_open(&m_handle, "default", SND_PCM_STREAM_PLAYBACK, 0)) < 0) {
    Log::error("Playback open error:\n" + std::string(snd_strerror(err)) + "\n");
    m_asoundinit = false;
  }
  
  if ((err = snd_pcm_set_params(m_handle,
				SND_PCM_FORMAT_FLOAT_LE,
				SND_PCM_ACCESS_RW_INTERLEAVED,
				m_channels,
				m_samplerate,
				1,
				500000)) < 0) {   /* 0.5sec */
    Log::error("Playback open error:" + std::string(snd_strerror(err)) + "\n");
    m_asoundinit = false;   
  }
}

int Stream::sendAsound(float *buffer,int len)
{
  return snd_pcm_writei(m_handle, buffer, len/m_channels);
}

void Stream::initSocket(const char * ip , unsigned short port)
{
  m_socketinit = true;
  m_socket = socket(AF_INET, SOCK_STREAM, 0 );
  
  m_addrConnect.sin_family = AF_INET ;
  m_addrConnect.sin_addr.s_addr = inet_addr(ip);
  m_addrConnect.sin_port = htons (port);
  
  if(connect(m_socket, (struct sockaddr*)&m_addrConnect, sizeof(m_addrConnect)) == -1){
    Log::error( "connect() error");
    m_socketinit = false;
  }
}

int Stream::sendCommand(bool start)
{
  if(m_socket == -1)
	return -1;
  unsigned char cmd[] = {'C','F','?','?','S','?'};
  
  cmd[2] = m_sfinfo.samplerate>>8;
  cmd[3] = m_sfinfo.samplerate&0x00FF;
  if(start)
    cmd[5] = 1;
  else
    cmd[5] = 0;
	
  int len;
  if( (len = send(m_socket,cmd,6,0)) < 0)
    {
      Log::error("Command send fail.");
      return 0;
    }
  return len;
}

void Stream::wait()
{
  char buffer[10];
  int n;
  do { n=read(m_socket,buffer,10); } while(n != 10);
}

int Stream::sendSocket(float *buffer, int len)
{
  int len_;
  if( (len_ = send(m_socket,buffer,len,0)) < 0)
    {
      Log::error("Send fail.");
      return 0;
    }
  return len_;
}

bool Stream::openFile(std::string filepath,Data *data)
{
  std::string path = "/var/ftp/"+filepath;
  if (! (m_file = sf_open (path.c_str(), SFM_READ, &m_sfinfo)))
    {
      Log::error("Can't open file: " + path);
      return false;
    }
  m_samplerate = m_sfinfo.samplerate;
  m_channels = m_sfinfo.channels;
  int frames = data->getOffset();
  if(data->isFrametype_ms()){
	std::cout << "m_samplerate" << m_samplerate << std::endl;
	std::cout << "frames" << frames << std::endl;
	std::cout << "m_channels" << m_channels << std::endl;
	float tmp = 1.*m_samplerate*frames/1000.;
	std::cout << "tmp" << tmp << std::endl;
	frames = static_cast<int>(floor(tmp));
	std::cout << "frames" << frames << std::endl;
  }

  this->setPos(frames);

  return true;
}

void Stream::setPos(int frames)
{
  sf_seek(m_file,frames,SEEK_SET);
}

int Stream::getFileSamples(float *buffer,int len)
{
  int readcount = sf_read_float (m_file, buffer, len);
  if(readcount == 0)
    {
      sf_close(m_file);
      return -1;
    }
  return readcount;
}

void Stream::closeSocket()
{
  shutdown(m_socket,2);
  close(m_socket);
}
