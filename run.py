import subprocess
import sys
import httplib, urllib
proc = subprocess.Popen(sys.argv[1:],stdout=subprocess.PIPE)

while True:
	message = proc.stdout.readline().split("|")
	if len(message) < 2:
		continue
	params = urllib.urlencode({'from': "module "+message[1], 'type': message[0], 'value': message[2]})
	headers = {"Content-type": "application/x-www-form-urlencoded","Accept": "text/plain"}
	
	conn = httplib.HTTPConnection("192.168.1.12:80")
	conn.request("POST", "/setlog", params, headers)
	response = conn.getresponse()
	conn.close()
