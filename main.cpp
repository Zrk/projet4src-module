#include <stdio.h>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <string.h>
#include <math.h>


#include <sndfile.h>
#include <alsa/asoundlib.h>

#include "pipe.hpp"
#include "stream.hpp"

#define DATALEN 64*8
//#define LOCAL

char device[] = "default";

int main (int argc, char **argv)
{
  
  std::string ip = "127.0.0.1";
  std::string room = "kitchen";
  int port = 2222;
  bool local = false;
  bool local_log = true;
  for (int i = 1; i < argc; i++)  /* Skip argv[0] (program name). */
    {
      if (strcmp(argv[i], "-ip") == 0)  /* Process optional arguments. */
        {
	  if (i + 1 <= argc - 1)  /* There are enough arguments in argv. */
	    ip = std::string(argv[i+1]);
	  else
            {
	      std::cout << "usage: module_streaming -ip 1.1.1.1 -port 2222 -room kitchen (-local)" << std::endl;
	      exit(0);
            }
        }
	
      if (strcmp(argv[i], "-port") == 0)  /* Process optional arguments. */
        {
	  if (i + 1 <= argc - 1)  /* There are enough arguments in argv. */
	    port = atoi(argv[i+1]);
	  else
            {
	      std::cout << "usage: module_streaming -ip 1.1.1.1 -port 2222 -room kitchen (-local)" << std::endl;
	      exit(0);
            }
        }

      if (strcmp(argv[i], "-room") == 0)  /* Process optional arguments. */
        {
	  if (i + 1 <= argc - 1)  /* There are enough arguments in argv. */
	    room = std::string(argv[i+1]);
	  else
            {
	      std::cout << "usage: module_streaming -ip 1.1.1.1 -port 2222 -room kitchen (-local)" << std::endl;
	      exit(0);
            }
        }
	
      if (strcmp(argv[i], "-local") == 0){
	local = true;
	}
      if (strcmp(argv[i], "-rl") == 0){
	local_log = false;
	}
      if (strcmp(argv[i], "-help") == 0){
	std::cout << "usage: module_streaming -ip 1.1.1.1 -port 2222 -room kitchen (-local)" << std::endl;
	return 0;
      }
    }
  Log::local = local_log;
  Log::room = room;
  std::string path;
  
  path =  "../projet4src-server/server/pipes/";
  path += room;
  path +=  ".pipe";
  Data *data = new Data(path);
  Stream *stream = new Stream(1);
  float buffer [DATALEN];

  path =  "../projet4src-server/server/times/";
  path += room;
  path +=  ".time";

  // create file if it doesn't existe
  std::ofstream file_frame(path, std::ios::out | std::ios::trunc);
  file_frame << -1;
  file_frame.close();

  if(!local)
	stream->initSocket(ip.c_str(),port);

  while(!data->loadData()); // wait for data
  while(true)
    {
      std::string filepath;
      if(data->getFilepath(&filepath))
	{
	  if(!stream->openFile(filepath,data))
	    continue;
	  if(!local)
	    std::cout << stream->sendCommand(true) << std::endl;

	  stream->initAsound();
	  if(data->isFrametype_ms())
		  Log::info("Start playing: " +filepath + " at frame: " + std::to_string(data->getOffset()) + "ms");
	  else
		  Log::info("Start playing: " +filepath + " at frame: " + std::to_string(data->getOffset()));
	  bool finished = true;
	  while(stream->getFileSamples(buffer,DATALEN) > 0)
	    {
		for (int i = 0 ; i < DATALEN ; i ++){
			buffer[i] = buffer[i]*data->getVolume();
		        /*float acc=0;
			std::cout << "data->m_filtercoeffs_nb "  << data->m_filtercoeffs_nb << std::endl;
 			for (unsigned int k = 0 ; k < data->m_filtercoeffs_nb; k++)
				acc += data->m_filtercoeffs[k]*buffer[i-k]*81.;
			buffer[i] = acc;
			std::cout << "acc "  << acc << std::endl;*/
		}
	      if(!local){
		stream->sendSocket(buffer,DATALEN*4);
	      
		//stream->sleep(DATALEN);
		stream->wait();
	      } else {
		stream->sendAsound(buffer,DATALEN);	
	      }
	      
	      std::ofstream file_frame(path, std::ios::out | std::ios::trunc);
	      file_frame << stream->getPos()+DATALEN/2;
	      file_frame.close();
               
 	      data->resetOffset();
	      if(data->loadData() and data->hasChanged()){
		  finished = false;
		  break;
	    	}
	    }
	  if(!finished)
		  Log::info("Stop playing: " +filepath + " at frame: " + std::to_string(static_cast<int>(stream->getPos())));
	  else{
		  Log::info("Stop playing: " +filepath + " at the end");
		  std::ofstream file_frame(path, std::ios::out | std::ios::trunc);
		  file_frame << -1;
		  file_frame.close();
		  data->go2Next();
	  }
	  stream->closeFile();
	  if(!local)
	     stream->sendCommand(false);
	}
      else
	{
	  data->loadData();
	}
    } 
  return 0 ;
}
