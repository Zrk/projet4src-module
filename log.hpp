#ifndef H_LOG
#define H_LOG
#include <iostream>
#include <string>
#include <time.h>

const std::string currentDateTime();

class Log{

public:
  static bool local;
  static std::string room;
  static void info(std::string val);
  static void warning(std::string val);
  static void error(std::string val);
};


#endif // H_LOG
