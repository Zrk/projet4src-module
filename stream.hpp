#ifndef H_STREAM
#define H_STREAM

#define SEND_SOCKET 0b00000001
#define SEND_LOCAL  0b00000010

#include <iostream>
#include <sndfile.h>
#include <alsa/asoundlib.h>
#include <sys/socket.h>
#include <linux/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <math.h>
#include "log.hpp"
#include "pipe.hpp"

class Stream
{
public:
  Stream(int isSocket){};
  ~Stream(){}
  
  bool openFile(std::string filepath,Data *data);
  int getFileSamples(float *buffer,int len);
  void setPos(int frames);
  int getPos() {return sf_seek(m_file, 0, SEEK_CUR);}
  
  void initSocket(const char *ip,unsigned short port);
  int sendSocket(float *buffer, int len);
  int sendCommand(bool start);
  void closeSocket();
  void wait();
  void initAsound();
  int sendAsound(float *buffer, int len);
  void closeFile(){sf_close (m_file);}
  void sleep(int len) { usleep ( (int) floor( len / (m_channels*1.) * 1 / (m_samplerate*1.)*1000000)); }
private:
  bool m_socketinit;
  bool m_asoundinit;
  int m_samplerate;
  int m_channels;
  
  int m_lendatatopush;
  
  int m_socket;
  struct sockaddr_in m_addrConnect;
  
  SNDFILE *m_file;
  SF_INFO m_sfinfo ;
  snd_pcm_t *m_handle;
  snd_pcm_sframes_t m_frames;
};

#endif // H_STREAM
