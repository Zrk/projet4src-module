#include "log.hpp"

// Get current date/time, format is YYYY-MM-DD.HH:mm:ss
const std::string currentDateTime() {
  time_t     now = time(0);
  struct tm  tstruct;
  char       buf[80];
  tstruct = *localtime(&now);
  // Visit http://en.cppreference.com/w/cpp/chrono/c/strftime
  // for more information about date/time format
  strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);

  return buf;
}

bool Log::local = false;
std::string Log::room = "";

void Log::info(std::string val){
  if(Log::local)
	  std::cout << "[info at " << currentDateTime() << " " << Log::room <<"] " << val << std::endl;
  else
	  std::cout << "I|" << Log::room << "|" << val << std::endl;
}

void Log::error(std::string val){
  if(Log::local)
	  std::cout << "[error at " << currentDateTime() << " " << Log::room <<"] " << val << std::endl;
  else
	  std::cout << "E|" << Log::room << "|" << val << std::endl;
}

void Log::warning(std::string val){
  if(Log::local)
	  std::cout << "[warning at " << currentDateTime() << " " << Log::room <<"] " << val << std::endl;
  else
	  std::cout << "W|" << Log::room << "|" << val << std::endl;
}
