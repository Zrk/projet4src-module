#ifndef H_PIPE
#define H_PIPE

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <unistd.h> //pipe
#include <fcntl.h> //open
#include <cstdint>
#include <sys/stat.h>
#include <jsoncpp/json/json.h>

#include "log.hpp"

class Data
{ 

public:
  Data(std::string pipepath);
  ~Data(){}
  
  bool loadData();
  bool hasChanged()
  {
    return m_hasChanged;
  }
  float getVolume(){return m_volume;}
  bool getFilepath(std::string *filepath){
	if(m_filepath == "")
		return false;
	*filepath = m_filepath;
	return true;
  }
  void go2Next(){m_filepath="";}
  int getOffset(){return m_frames;}
  bool isFrametype_ms(){return m_framestype_ms;}
  void getSamples();
  void resetOffset(){m_frames = 0;}
  float *m_filtercoeffs;
  unsigned int m_filtercoeffs_nb;
private:
  int m_pipefiledescriptor;
  float m_volume;
  unsigned int m_frames;
  bool m_framestype_ms;
  std::string m_filepath;
  Log log;
  bool m_hasChanged;
};

#endif //H_PIPE
