LIBS = -lasound -lsndfile -ljsoncpp

SRC = *.cpp
TARGET = module_streaming

$(TARGET): $(SRC)
	g++ -o $(TARGET) $^ $(LIBS) -std=c++11 -g -Wall

%.o: %.cpp
	g++ -c $< -g -Wall
