#include "pipe.hpp"
using namespace std;
  
Data::Data(string pipepath): m_volume(1.0),m_frames(0),m_framestype_ms(false),m_filtercoeffs(NULL),m_filtercoeffs_nb(0)
{
  m_pipefiledescriptor = open(pipepath.c_str(),O_NONBLOCK | O_RDONLY);
  if(m_pipefiledescriptor > 0)
    Log::info("Pipe " + pipepath + " open");
  else
    Log::error("Fail when opening pipe");
}

bool Data::loadData()
{
  char buffer[4096*3] = {0};
  int nr_read;  
  nr_read = read(m_pipefiledescriptor, buffer, 4096*3);
  if(nr_read == 0 or nr_read == -1)
    return false;

  /*ofstream myfile;
  myfile.open ("last_order.json");
  myfile << buffer;
  myfile.close();*/
  
  Log::info("Incomming data from pipe : " + std::string(buffer));
  
  /* Decode Data */  
  Json::Value root;   // will contains the root value after parsing.
  Json::Reader reader;
  bool parsingSuccessful = reader.parse(std::string(buffer), root);
  if ( !parsingSuccessful )
    {
      // report to the user the failure and their locations in the document.
      std::cout  << "Failed to parse configuration\n"
		 << reader.getFormattedErrorMessages();
      return false;
    }
  m_frames = root.get("frames", m_frames).asUInt();
  m_framestype_ms = root.get("frames_type_ms", false).asBool();
  if(m_framestype_ms){
      std::cout  << "m_framestype_ms = true\n";
  } else {
      std::cout  << "m_framestype_ms = false\n";
  }
  m_volume = root.get("volume", m_volume).asFloat();

  std::string filepath = root.get("current_music", m_filepath).asString();

  m_hasChanged = false;
  if(filepath != m_filepath)
    m_hasChanged = true;
  m_filepath = filepath;
  return true;
}

void Data::getSamples(){
  std::string line;
  ifstream myfile ("../projet4src-server/server/filtercoeffs.json");

  if (myfile.is_open())
  {
    getline (myfile,line);
    cout << line << '\n';
  /* Decode Data */  
  Json::Value root;   // will contains the root value after parsing.
  Json::Reader reader;
  bool parsingSuccessful = reader.parse(line, root);
  if ( !parsingSuccessful )
    {
      // report to the user the failure and their locations in the document.
      std::cout  << "Failed to parse configuration\n"
		 << reader.getFormattedErrorMessages();
      return;
    }
  const Json::Value coeffs = root["coeffs"];
  m_filtercoeffs = new float[coeffs.size()];
  for ( unsigned int index = 0; index < coeffs.size(); ++index ){  // Iterates over the sequence elements.
     m_filtercoeffs[index] = coeffs[index].asFloat();
     std::cout << coeffs[index].asFloat() << std::endl;
  }
  m_filtercoeffs_nb = coeffs.size();
  }
}

